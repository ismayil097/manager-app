import React, { Component } from 'react';
import {
  Text,
  View,
  Navigator,
  NativeModules,
} from 'react-native';
import { COLOR, ThemeProvider, Toolbar } from 'react-native-material-ui';
import { Provider } from 'react-redux'
import { createStore, applyMiddleware } from 'redux'
import ReduxThunk from 'redux-thunk'
import firebase from 'firebase'
import reducers from './reducers'
import Router from './Router'

const uiTheme = {
  palette: {
    primaryColor: COLOR.green500,
  },
  toolbar: {
    container: {
      height: 50,
    },
  },
};

class App extends Component {

  componentWillMount() {
    const config = {
      apiKey: "AIzaSyCNgs7eWfm_HUlbbmqDVcTeTnKJn5yNIO4",
      authDomain: "manager-718df.firebaseapp.com",
      databaseURL: "https://manager-718df.firebaseio.com",
      projectId: "manager-718df",
      storageBucket: "manager-718df.appspot.com",
      messagingSenderId: "958147940803"
    };
    if (!firebase.apps.length) {
      firebase.initializeApp(config);
    }
  }

  render() {
    const store = createStore(reducers, {}, applyMiddleware(ReduxThunk))
    return (
      <ThemeProvider uiTheme={uiTheme}>
        <Provider store={store}>
          <Router />  
        </Provider>
      </ThemeProvider>
    );
  }
}

export default App;


