import React from 'react'
import { Scene, Router } from 'react-native-router-flux'
import LoginForm from './Components/LoginForm'
import StudentList from './Components/studentList'

const RouterComponent = () => {
    return (
        <Router sceneStyle={{ paddingTop: 60 }}>
            <Scene key="auth">
                <Scene key="login" component={LoginForm} title="Please Login" />
            </Scene>

            <Scene key="main"> 
                <Scene 
                    onRight={console.log('dasdas')}
                    rightTitle="Add"
                    key="studentList" 
                    component={StudentList} 
                    title="Students" 
                />
            </Scene>
        </Router>
    );
}

export default RouterComponent;

