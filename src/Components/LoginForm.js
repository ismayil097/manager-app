import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Card, CardSection, Input, Spinner } from '../Components/Common'
import { Button, } from 'react-native-material-ui'
import { View, Text } from 'react-native'
import { emailChanged, passwordChanged, loginUser } from '../actions'

const styles = {
    buttonStyle: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },
    errorTextStyle: {
        fontSize: 20,
        alignSelf: 'center',
        color: 'red',
    }
}

class LoginForm extends Component {

    onEmailChange = text => {
        this.props.emailChanged(text);
    }
    onPasswordChange = text => {
        this.props.passwordChanged(text);
    }

    onButtonPress = () => {
        const { email, password } = this.props;

        this.props.loginUser({ email, password })
    }

    renderButton = () => {
        if (this.props.loading) {
            return <Spinner size='large' />
        }
        return (
            <Button onPress={this.onButtonPress} raised primary text="Login" />
        )
    }

    render() {
        return (
            <Card>
                <CardSection>
                    <Input
                        label="Email"
                        placeholder="email@gmail.com"
                        onChangeText={this.onEmailChange}
                        value={this.props.email}
                    />
                </CardSection>
                <CardSection>
                    <Input
                        secureTextEntry
                        label="Password"
                        placeholder="password"
                        onChangeText={this.onPasswordChange}
                        value={this.props.password}
                    />
                </CardSection>
                <Text style={styles.errorTextStyle}>
                    {this.props.error}
                </Text>
                <CardSection >
                    <View style={styles.buttonStyle}>
                        {this.renderButton()}
                    </View>
                </CardSection>
            </Card>
        )
    }
}

const mapStateToProps = state => {
    return {
        email: state.auth.email,
        password: state.auth.password,
        error: state.auth.error,
        loading: state.auth.loading
    }
}

export default connect(mapStateToProps, {
    emailChanged, passwordChanged, loginUser
})(LoginForm);


